package com.example.demo.Interceptor;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自定义异常
 *
 * @author : YZD
 * @date : 2021-7-28 16:13
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BzException extends RuntimeException {

    private Integer status;
    private String message;

    public BzException(BzException resultEnum) {
        super(resultEnum.getMessage());
        this.status = resultEnum.getStatus();
    }

    public BzException(String message) {
        this.status = ResponseEnum.SERVER_ERROR.getStatus();
        this.message = message;
    }

    public BzException(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

}
